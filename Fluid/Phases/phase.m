% Phase class
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DARSim 2 Reservoir Simulator
%Author: Matteo Cusini, Yuhang Wang
%TU Delft
%Created: 14 July 2016
%Last modified: 6 Jan 2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef phase < matlab.mixin.Heterogeneous & handle
    properties
        mu % Reference Viscosity
        sr % Irriducible saturation
        
        % gridded interpolant object for data
        % Rs and zcf are pressure dependent
        Rs_p_table;
        dRs_p_table;
        zcf_p_table; % attn. zcf: compressibility factor; z: mole(mass) fraction of a component;
        dzcf_p_table;
    end
    methods
        function obj = phase()
            % obj.ComputeLookupTable();
        end
        function [Upwind, U] = UpWindAndRockFluxes(obj, Grid, P, RhoInt)
            switch class(Grid)
                case('corner_point_grid')
                    Neighbor1Index = Grid.CornerPointGridData.Internal_Faces.CellNeighbor1Index;
                    Neighbor2Index = Grid.CornerPointGridData.Internal_Faces.CellNeighbor2Index;
                    
                    % Gravitational velocities
                    Ug = - ( Grid.Depth(Neighbor2Index) - Grid.Depth(Neighbor1Index) ) .* RhoInt;
                    
                    % Compute 'rock' fluxes ([m^3/s])
                    U = - Grid.Trans .* ( P(Neighbor2Index)-P(Neighbor1Index) - Ug );
                    
                    % Use velocity to build upwind operator
                    nc = Grid.N;
                    nf = length(Grid.Trans);
                    C = (U>=0) .* Grid.CornerPointGridData.Internal_Faces.CellNeighbor1Index + ...
                        (U< 0) .* Grid.CornerPointGridData.Internal_Faces.CellNeighbor2Index;
                    Upwind = sparse( (1:nf)', C , ones(nf,1) , nf , nc );
                    
                case('cartesian_grid')
                    Nx = Grid.Nx;
                    Ny = Grid.Ny;
                    Nz = Grid.Nz;
                    N = Grid.N;

                    % Gravitational velocities
                    depth = reshape(Grid.Depth, Nx, Ny, Nz);
                    Ugx = zeros(Nx+1, Ny, Nz);
                    Ugy = zeros(Nx, Ny+1, Nz);
                    Ugz = zeros(Nx, Ny, Nz+1);
                    Ugx(2:Nx,:,:) = (depth(1:Nx-1,:,:) - depth(2:Nx,:,:)) .* RhoInt.x(2:Nx,:,:);
                    Ugy(:,2:Ny,:) = (depth(:,1:Ny-1,:) - depth(:,2:Ny,:)) .* RhoInt.y(:,2:Ny,:);
                    Ugz(:,:,2:Nz) = (depth(:,:,1:Nz-1) - depth(:,:,2:Nz)) .* RhoInt.z(:,:,2:Nz);
                    
                    P = reshape(P, Nx, Ny, Nz);
                    %% Compute 'rock' fluxes ([m^3/s])
                    U.x = zeros(Nx+1,Ny,Nz);
                    U.y = zeros(Nx,Ny+1,Nz);
                    U.z = zeros(Nx,Ny,Nz+1);
                    U.x(2:Nx,:,:) = Grid.Tx(2:Nx,:,:) .* ( (P(1:Nx-1,:,:)-P(2:Nx,:,:)) - Ugx(2:Nx,:,:) );
                    U.y(:,2:Ny,:) = Grid.Ty(:,2:Ny,:) .* ( (P(:,1:Ny-1,:)-P(:,2:Ny,:)) - Ugy(:,2:Ny,:) );
                    U.z(:,:,2:Nz) = Grid.Tz(:,:,2:Nz) .* ( (P(:,:,1:Nz-1)-P(:,:,2:Nz)) - Ugz(:,:,2:Nz) );
                    
                    %% Use velocity to build upwind operator
                    L = reshape((U.x(2:Nx+1,:,:) >= 0), N, 1);
                    R = reshape((U.x(1:Nx,:,:) < 0), N, 1);
                    B = reshape((U.y(:,2:Ny+1,:) >= 0), N, 1);
                    T = reshape((U.y(:,1:Ny,:) < 0), N, 1);
                    Down = reshape((U.z(:,:,2:Nz+1) >= 0), N, 1);
                    Up = reshape((U.z(:,:,1:Nz) < 0), N, 1);
                    
                    DiagVecs = [L, R];
                    DiagIndx = [0, 1];
                    Upwind.x = spdiags(DiagVecs, DiagIndx, N, N);
                    DiagVecs = [B, T];
                    DiagIndx = [0, Nx];
                    Upwind.y = spdiags(DiagVecs, DiagIndx, N, N);
                    DiagVecs = [Down, Up];
                    DiagIndx = [0, Nx*Ny];
                    Upwind.z = spdiags(DiagVecs, DiagIndx, N, N);
            end
        end
        
        % Calculate Rs, dRs, zcf, dzcf table;
%         function ComputeLookupTable(obj)
%             % compressibility
%             syms V;
%             R = 83.1447; % bar cm^3 mol^(-1) K^(-1);
%             T_C = 65; % temperature in C;
%             T = T_C + 273.15; % temperature in K;
%             a_CO2 = 7.54e7 - 4.13e4*T; % bar cm^6 K^(0.5) mol^(-2);
%             b_CO2 = 27.8; % cm^3 mol^(-1);
% 
%             salinity = 100e3; % ppm (1000 ppm means 1g solute in 1kg (1 liter) water); 4*58.44e3 = 4 mol/L
%             m_Na = salinity/1e3/58.44; % mol kg^(-1);
%             m_Cl = salinity/1e3/58.44; % mol kg^(-1);
%             % m_Na = 1; % mol kg^(-1);
%             % m_Cl = 1; % mol kg^(-1);
% 
%             p = (2:2:400)'; % bar;
%             Z = zeros(length(p),1);
%             Vm = zeros(length(p),1);
% 
%             for i = 1:length(p)
%                 S = vpasolve(V^3-V^2*(R*T/p(i))-V*(R*T*b_CO2/p(i) - a_CO2/(p(i)*T^0.5) + b_CO2^2) - (a_CO2*b_CO2/(p(i)*T^0.5)) == 0, V);
%                 Vm(i) = max(double(S(imag(S)==0)));
% 
%                 Z(i) = p(i)*Vm(i)/(R*T);
%             end
%             dzcf_value = [(Z(2)-Z(1))/(p(2)*10^5-p(1)*10^5); (Z(2:end)-Z(1:end-1))./(p(2:end)*10^5-p(1:end-1)*10^5)]; % convert bar to pa
%             
%             obj.zcf_p_table = griddedInterpolant(p*10^5,Z); % convert bar to pa
%             obj.dzcf_p_table = griddedInterpolant(p*10^5,dzcf_value); % convert bar to pa
%             
%             % fugacity
%             a_H2O_CO2 = 7.89e7; % bar cm^6 K^(0.5) mol^(-2);
%             b_H2O = 18.18; % cm^3 mol^(-1);
% 
%             phi_CO2 = exp(log(Vm./(Vm-b_CO2)) + (b_CO2./(Vm-b_CO2)) - 2*(a_CO2./(R*T^(1.5)*b_CO2))*log((Vm+b_CO2)./Vm) + (a_CO2*b_CO2./(R*T^(1.5)*b_CO2^2))*(log((Vm+b_CO2)./Vm)-(b_CO2./(Vm+b_CO2))) - log((p.*Vm)./(R*T)));
%             phi_H2O = exp(log(Vm./(Vm-b_CO2)) + (b_H2O./(Vm-b_CO2)) - 2*(a_H2O_CO2./(R*T^(1.5)*b_CO2))*log((Vm+b_CO2)./Vm) + (a_CO2*b_H2O./(R*T^(1.5)*b_CO2^2))*(log((Vm+b_CO2)./Vm)-(b_CO2./(Vm+b_CO2))) - log((p.*Vm)./(R*T)));
% 
%             % equilibrium constants
%             K_CO2_0_g = 10.^(1.189 + 1.304e-2*T_C - 5.446e-5*(T_C)^2);
%             K_H2O_0 = 10.^(-2.209 + 3.097e-2*T_C - 1.098e-4*(T_C)^2 + 2.048e-7*(T_C)^3);
%             
%             % CO2 molality in pure water
%             p_ref = 1; % bar;
% 
%             Vm_CO2_g_avg = 32.6; % cm^3 mol^(-1);
%             Vm_H2O_avg = 18.1; % cm^3 mol^(-1);
% 
%             A = K_H2O_0./(phi_H2O.*p).*exp((p-p_ref)*Vm_H2O_avg./(R*T));
%             B = phi_CO2.*p./(55.508*K_CO2_0_g).*exp(-(p-p_ref)*Vm_CO2_g_avg./(R*T));
% 
%             y_H2O = (1-B)./(1./A-B);
%             x_CO2 = B.*(1-y_H2O);
% 
%             m_CO2_0 = 55.508.*x_CO2./(1-x_CO2);
%             
%             % CO2 activity coefficient
%             par = @(c_1,c_2,c_3,c_4,c_5,c_6,T,p) c_1 + c_2*T + c_3/T + c_4*p./T + c_5*p./(630-T) + c_6*T.*log(p); 
% 
%             gamma_CO2 = exp(2*m_Na*par(-0.411370585,6.07632013e-4,97.5347708,-0.0237622469,0.0170656236,1.41335834e-5,T,p)...
%                         + m_Na*m_Cl*par(3.36389723e-4,-1.98298980e-5,0,2.122220830e-3,-5.24873303e-3,0,T,p));   
%                     
%             % CO2 molality in brines
%             m_CO2 = m_CO2_0./gamma_CO2;
%             x_CO2 = m_CO2./(m_CO2 + 55.508 + 2*m_Na);
%             
%             % brine density
%             a_1 = 5.916365 - 0.01035794*T + 0.9270048e-5*T^2 - 1127.522/T + 100674.1/(T^2);
%             a_2 = 0.5204914e-2 - 0.10482101810e-4*T + 0.8328532e-8*T^2 - 1.1702939/T + 102.2783/(T^2);
%             a_3 = 0.118547e-7 - 0.6599143e-10*T;
%             a_4 = -2.5166 + 0.0111766*T - 0.170552e-4*T^2;
%             a_5 = 2.84851 - 0.0154305*T + 0.223982e-4*T^2;
%             a_6 = -0.0014814 + 0.82969e-5*T - 0.12469e-7*T^2;
%             a_7 = 0.0027141 - 0.15391e-4 + 0.22655e-7*T^2;
%             a_8 = 0.62158e-6 - 0.40075e-8*T + 0.65972e-11*T^2;
% 
%             S = salinity*1.0e-3/(salinity*1.0e-3 + 1000); % NaCl mass fraction in brine;
%             % p_f = 1.01972*p; % pressure in kgf/(cm^2)
%             p_f = 1.0332; % standard condition;
% 
%             rho_brine = ((a_1 - p_f*a_2 - p_f.^2*a_3 + a_4*S + a_5*S^2 - p_f*a_6*S - p_f*a_7*S^2 - 0.5*p_f.^2*a_8*S)*1.0e-3).^(-1);
% 
%             % brine molar density at standard condition
%             n_NaCl = salinity/1e3/58.44;
%             rho_brine_molar_STC = rho_brine/((58.44*(n_NaCl/(n_NaCl*2+55.508)) + 18.052*(55.508/(n_NaCl*2+55.508)))*1.0e-3);
% 
%             % solution gas-oil ratio;
%             Rs_value = rho_brine_molar_STC*x_CO2./(44.990*(1-x_CO2));
%             dRs_value = [(Rs_value(2)-Rs_value(1))/(p(2)*10^5-p(1)*10^5); (Rs_value(2:end)-Rs_value(1:end-1))./(p(2:end)*10^5-p(1:end-1)*10^5)]; % convert bar to pa
%             
%             obj.Rs_p_table = griddedInterpolant(p*10^5,Rs_value); % convert bar to pa
%             obj.dRs_p_table = griddedInterpolant(p*10^5,dRs_value); % convert bar to pa
%         end
        
    end
end